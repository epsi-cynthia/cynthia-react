import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {Header, Redirect} from 'components';
import {Home, Message, Profile} from 'pages';
import './App.scss';

export const App: React.FC = () => {
    return (
        <Router>
            <Header/>
            <main role="main">
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/message" component={Message} />
                    <Route path="/profile" component={Profile} />
                    <Redirect Home />
                </Switch>
            </main>
        </Router>
    );
};

