import React from 'react';
import {useKeycloak} from 'utils/keycloak';

export const Profile = () => {
    // @ts-ignore
    const {name, email} = useKeycloak().userInfo;

    return (
        <section role='main' className="container">
            <h3>{name}</h3>
            <p>{email}</p>
        </section>
    );
};
