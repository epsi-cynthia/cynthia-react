import React, {FormEvent, useEffect, useState} from 'react';
import {PostComponent, usePost} from 'features/post';
import {useKeycloak} from 'utils/keycloak';
import M from 'materialize-css';

export const Home = () => {
    const {posts, savePost, postError} = usePost();
    // @ts-ignore
    const {given_name} = useKeycloak().userInfo;
    const [title, setTitle] = useState("");
    const [content, setContent] = useState("");

    useEffect(() => {
        const textarea = document.querySelectorAll('textarea');
        M.CharacterCounter.init(textarea);
    }, []);

    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        savePost({title, content});
        setTitle("");
        setContent("");
    };

    return (
        <section role="main" className="row">
            <div className="col s12 m6 offset-m3">
                <h4>Quoi de neuf {given_name} ?</h4>
                <form onSubmit={ handleSubmit }>
                    <div className="input-field">
                        <i className="material-icons prefix">mode_edit</i>
                        <input id="title" type="text" onChange={e => setTitle(e.target.value)} value={title}/>
                        <label htmlFor="title">Titre</label>
                    </div>
                    <div className="input-field">
                        <textarea id="content" className="materialize-textarea" data-length={255} onChange={e => setContent(e.target.value)} value={content}/>
                        <label htmlFor="content">Contenu</label>
                    </div>
                    <button className="btn right">Enregistrer</button>
                </form>
                {posts &&
                posts.map(post => {
                    return <PostComponent post={post} key={post.id} />
                })
                }
                {postError && <p>{postError}</p>}
            </div>
        </section>
    );
};
