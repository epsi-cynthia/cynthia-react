import {default as interaxios} from 'axios';
import {keycloak} from 'utils/keycloak/keycloak';

interaxios.interceptors.request.use(function (config) {
    if (keycloak.token != null) {
        config.headers.Authorization = `Bearer ${keycloak.token}`;
    }

    return config;
}, function (err) {
    return Promise.reject(err);
});

export {
    interaxios
};
