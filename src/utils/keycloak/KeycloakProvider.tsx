import React, {useEffect, useState} from 'react';
import {keycloak} from './keycloak';
import {KeycloakInstance} from 'keycloak-js';

type KeycloakContextType = {
    keycloak: KeycloakInstance<'native'>,
    isInitialized: boolean
}

type KeycloakProviderProps = {
    children: React.ReactNode,
    LoadingComponent?: React.FC
}

export const KeycloakContext = React.createContext<KeycloakContextType>({keycloak, isInitialized: false});
export const KeycloakProvider = (props: KeycloakProviderProps) => {
    const {LoadingComponent} = props;
    const [isInitialized, setInitialized] = useState(false);

    useEffect(() => {
        keycloak.init({
            onLoad: 'login-required',
            silentCheckSsoRedirectUri: window.location.origin + '/silent-check-sso.html',
            promiseType: 'native'
        }).then(authenticated => {
            if (authenticated) {
                keycloak.loadUserInfo().then(() => {
                    setInitialized(true);
                });
            } else {
                setInitialized(true);
            }
        });
    }, []);

    if (!isInitialized && !!LoadingComponent) {
        return <LoadingComponent />;
    }

    return (
        <KeycloakContext.Provider value={{keycloak, isInitialized}}>
            {props.children}
        </KeycloakContext.Provider>
    );
};
