import Keycloak from 'keycloak-js';

export const keycloak = Keycloak<'native'>({
    url: "http://keycloak.cynthia.team/auth",
    realm: "master",
    clientId: "front-cynthia"
});
