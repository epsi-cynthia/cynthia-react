import {useContext} from 'react';
import {KeycloakContext} from './KeycloakProvider';

export const useKeycloak = () => {
    const {keycloak, isInitialized} = useContext(KeycloakContext);
    const userInfo = keycloak.userInfo;
    return {keycloak, isInitialized, userInfo};
};
