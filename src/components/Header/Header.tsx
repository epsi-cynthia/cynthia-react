import React from 'react';
import {Link} from 'react-router-dom';
import {useKeycloak} from 'utils/keycloak';

export const Header = () => {
    const {logout} = useKeycloak().keycloak;
    return (
        <header role="banner">
            <nav>
                <div className="nav-wrapper container">
                    <Link to="/" className="brand-logo">Logo</Link>
                    <ul id="nav-mobile" className="right hide-on-med-and-down">
                        <li><Link to="/message">Message</Link></li>
                        <li><Link to="/profile">Profile</Link></li>
                        <li><a onClick={logout}>Logout</a></li>
                    </ul>
                </div>
            </nav>
        </header>
    );
};
