import React from 'react';
import {Redirect as R} from 'react-router-dom';

type RedirectProps = {
    Home?: boolean,
    Login?: boolean,
    to?: string
}

export const Redirect = (props: RedirectProps) => {
    const {Home, Login, to} = props;
    return (
        <>
            {Home && <R to={{pathname: '/'}} />}
            {Login && <R to={{pathname: '/login'}} />}
            {to && <R to={{pathname: to}} />}
        </>
    )
};
