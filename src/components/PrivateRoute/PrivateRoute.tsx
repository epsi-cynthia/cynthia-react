import React from 'react';
import {useKeycloak} from 'utils/keycloak';
import {Route} from 'react-router-dom';
import {Redirect} from 'components';

type PrivateRouteProps = {
    path: string,
    component: React.FC
}

export const PrivateRoute = (props: PrivateRouteProps) => {
    const {path, component: Component} = props;
    const {authenticated} = useKeycloak().keycloak;

    return (
        <Route
            path={path}
            render={() => authenticated
                ? <Component />
                : <Redirect Login />}
        />
    )
};
