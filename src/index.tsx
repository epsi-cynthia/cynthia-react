import React from 'react';
import ReactDOM from 'react-dom';

// Import providers
import {store} from 'store';
import {Provider} from 'react-redux';
import {KeycloakProvider} from 'utils/keycloak';

import {App} from './App';
import {Loading} from 'components';

import * as serviceWorker from './serviceWorker';

import 'materialize-css/sass/materialize.scss';
import './index.scss';

ReactDOM.render(
    <KeycloakProvider LoadingComponent={Loading}>
        <Provider store={store}>
            <App />
        </Provider>
    </KeycloakProvider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
