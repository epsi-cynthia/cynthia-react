// Post Type
export type Post = {
    id?: string,
    title: string,
    content: string,
    authorId?: string,
    createdAt?: Date,
    updatedAt?: Date
}

export type PostState = {
    posts: Post[],
    isLoading: boolean,
    error?: PostError
}

// Errors
export type PostError =
    | typeof POST_ERROR_NOT_FOUND
    | typeof POST_ERROR_UNAUTHORIZED
    | typeof POST_ERROR_SERVER_ERROR

export const POST_ERROR_NOT_FOUND = "POST_ERROR_NOT_FOUND";
export const POST_ERROR_UNAUTHORIZED = "POST_ERROR_UNAUTHORIZED";
export const POST_ERROR_SERVER_ERROR = "POST_ERROR_SERVER_ERROR";

// Post Actions
export type PostAction =
    | FetchPostAction
    | FetchPostSuccessAction
    | FetchPostErrorAction
    | SavePostAction
    | SavePostSuccessAction
    | SavePostErrorAction
    | DeletePostAction
    | DeletePostSuccessAction
    | DeletePostErrorAction

// Fetch Post
export type FetchPostAction = {
    type: typeof FETCH_POST
}

export type FetchPostSuccessAction = {
    type: typeof FETCH_POST_SUCCESS,
    payload: Post[]
}

export type FetchPostErrorAction = {
    type: typeof FETCH_POST_ERROR,
    error: PostError
}

export const FETCH_POST = "FETCH_POST";
export const FETCH_POST_SUCCESS = "FETCH_POST_SUCCESS";
export const FETCH_POST_ERROR = "FETCH_POST_ERROR";

// Save Post
export type SavePostAction = {
    type: typeof SAVE_POST
}

export type SavePostSuccessAction = {
    type: typeof SAVE_POST_SUCCESS,
    payload: Post
}

export type SavePostErrorAction = {
    type: typeof SAVE_POST_ERROR,
    error: PostError
}

export const SAVE_POST = "SAVE_POST";
export const SAVE_POST_SUCCESS = "SAVE_POST_SUCCESS";
export const SAVE_POST_ERROR = "SAVE_POST_ERROR";

// Delete Post
export type DeletePostAction = {
    type: typeof DELETE_POST
}

export type DeletePostSuccessAction = {
    type: typeof DELETE_POST_SUCCESS,
    id: string
}

export type DeletePostErrorAction = {
    type: typeof DELETE_POST_ERROR,
    error: PostError
}

export const DELETE_POST = "DELETE_POST";
export const DELETE_POST_SUCCESS = "DELETE_POST_SUCCESS";
export const DELETE_POST_ERROR = "DELETE_POST_ERROR";
