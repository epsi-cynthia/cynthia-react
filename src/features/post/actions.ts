import {Dispatch} from 'redux';
import {
    DELETE_POST,
    DELETE_POST_ERROR,
    DELETE_POST_SUCCESS,
    DeletePostAction,
    DeletePostErrorAction,
    DeletePostSuccessAction,
    FETCH_POST,
    FETCH_POST_ERROR,
    FETCH_POST_SUCCESS,
    FetchPostAction,
    FetchPostErrorAction,
    FetchPostSuccessAction,
    Post,
    POST_ERROR_UNAUTHORIZED,
    PostError,
    PostState,
    SAVE_POST,
    SAVE_POST_ERROR,
    SAVE_POST_SUCCESS,
    SavePostAction,
    SavePostErrorAction,
    SavePostSuccessAction
} from './types';
import {AxiosInstance, AxiosResponse} from 'axios';

const url = "http://post.cynthia.team";

const fetchPostStart = (): FetchPostAction => {
    return {
        type: FETCH_POST
    }
};

const fetchPostSuccess = (payload: Post[]): FetchPostSuccessAction => {
    return {
        type: FETCH_POST_SUCCESS,
        payload
    }
};

const fetchPostError = (error: PostError): FetchPostErrorAction => {
    return {
        type: FETCH_POST_ERROR,
        error
    }
};

export const fetchPost = () => {
    return (dispatch: Dispatch, getState: PostState, interaxios: AxiosInstance) => {
        dispatch(fetchPostStart());
        interaxios.get(`${url}/posts`)
            .then((res: AxiosResponse) => {
                const posts: Post[] = [].concat(res.data);
                dispatch(fetchPostSuccess(posts))
            })
            .catch(() => {
                dispatch(fetchPostError(POST_ERROR_UNAUTHORIZED));
            });
    }
};

const savePostStart = (): SavePostAction => {
    return {
        type: SAVE_POST
    }
};

const savePostSuccess = (payload: Post): SavePostSuccessAction => {
    return {
        type: SAVE_POST_SUCCESS,
        payload
    }
};

const savePostError = (error: PostError): SavePostErrorAction => {
    return {
        type: SAVE_POST_ERROR,
        error
    }
};

export const savePost = (post: Post) => {
    return (dispatch: Dispatch, getState: PostState, interaxios: AxiosInstance) => {
        dispatch(savePostStart());
        interaxios.post(`${url}/posts`, post)
            .then(() => {
                dispatch(savePostSuccess(post))
            })
            .catch(() => {
                dispatch(savePostError(POST_ERROR_UNAUTHORIZED));
            });
    }
};

const deletePostStart = (): DeletePostAction => {
    return {
        type: DELETE_POST
    }
};

const deletePostSuccess = (id: string): DeletePostSuccessAction => {
    return {
        type: DELETE_POST_SUCCESS,
        id
    }
};

const deletePostError = (error: PostError): DeletePostErrorAction => {
    return {
        type: DELETE_POST_ERROR,
        error
    }
};

export const deletePost = (id: string) => {
    return (dispatch: Dispatch, getState: PostState, interaxios: AxiosInstance) => {
        dispatch(deletePostStart());
        interaxios.delete(`${url}/posts/${id}`)
            .then(() => {
                dispatch(deletePostSuccess(id))
            })
            .catch(() => {
                dispatch(deletePostError(POST_ERROR_UNAUTHORIZED));
            });
    }
};
