import React from 'react';
import {Post} from './types';
import './Post.scss';
import {useComment} from 'features/comment';
import {usePost} from 'features/post';
import {Comment} from '../comment/types';

type PostProps = {
    post: Post
}

type CommentProps = {
    comment: Comment
}

const deleteComment = (commentId: string) => {
    console.log(commentId);
    // suppression du commentaire
};

const CommentComponent = (props: CommentProps) => {
    const {comment} = props;

    return (
        <li className="collection-item">
            <div>
                {comment.message}
                <a href="#" onClick={() => deleteComment(comment.id)} className="secondary-content"><i className="material-icons">delete</i></a>
            </div>
        </li>
    )
};

const displayComments = (comments: Comment[]) => {
    if (!comments || !comments.length) return null;

    return (
        <ul className="collection with-header">
            <li className="collection-header"><h5>Commentaires :</h5></li>
            {
                comments.map(comment => {
                    return <CommentComponent comment={comment} key={comment.id} />
                })
            }
        </ul>
    )
};

export const PostComponent = (props: PostProps) => {
    const {post} = props;
    const {comments, fetchComment} = useComment();
    const {deletePost} = usePost();
    const dateTime = new Date(post.createdAt || "").toDateString();

    return (
        <div className="card" onClick={() => fetchComment(post.id || "")}>
            <div className="card-content">
                <h3 className="card-title">{post.title}<i className="material-icons right" onClick={() => deletePost(post.id || "")}>close</i></h3>
                <p>{post.content}</p>
                <time dateTime={dateTime}>{dateTime}</time>
                {displayComments(comments)}
            </div>
        </div>
    );
};
