import {
    FETCH_POST,
    FETCH_POST_SUCCESS,
    FETCH_POST_ERROR,
    SAVE_POST,
    SAVE_POST_SUCCESS,
    SAVE_POST_ERROR,
    DELETE_POST,
    DELETE_POST_SUCCESS,
    DELETE_POST_ERROR,
    PostAction,
    PostState
} from './types';

const initialState: PostState = {
    posts: [],
    isLoading: false
};

export const reducer = (state = initialState, action: PostAction) => {
    switch (action.type) {
        case FETCH_POST:
            return {
                ...state,
                isLoading: true
            };
        case FETCH_POST_SUCCESS:
            return {
                posts: action.payload,
                isLoading: false
            };
        case FETCH_POST_ERROR:
            return {
                posts: [],
                isLoading: false,
                error: action.error
            };
        case SAVE_POST:
            return {
                ...state,
                isLoading: true
            };
        case SAVE_POST_SUCCESS:
            return {
                posts: [...state.posts, action.payload],
                isLoading: false
            };
        case SAVE_POST_ERROR:
            return {
                posts: [],
                isLoading: false,
                error: action.error
            };
        case DELETE_POST:
            return {
                ...state,
                isLoading: true
            };
        case DELETE_POST_SUCCESS:
            return {
                posts: state.posts.filter(post => post.id != action.id),
                isLoading: false
            };
        case DELETE_POST_ERROR:
            return {
                posts: [],
                isLoading: false,
                error: action.error
            };
        default:
            return state;
    }
};
