import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {State} from 'store';
import {fetchPost, savePost, deletePost} from './actions';
import {Post} from './types';

export const usePost = () => {
    const dispatch = useDispatch();
    const posts = useSelector((state: State) => state.post.posts);
    const isLoading = useSelector((state: State) => state.post.isLoading);
    const error = useSelector((state: State) => state.post.error);

    useEffect(() => {
        dispatch(fetchPost());
    }, []);

    return {
        posts,
        postIsLoading: isLoading,
        postError: error,
        savePost: (post: Post) => dispatch(savePost(post)),
        deletePost: (id: string) => dispatch(deletePost(id))
    };
};
