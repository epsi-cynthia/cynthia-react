import {Dispatch} from 'redux';
import {
    FETCH_COMMENT,
    FETCH_COMMENT_ERROR,
    FETCH_COMMENT_SUCCESS,
    FetchCommentAction,
    FetchCommentErrorAction,
    FetchCommentSuccessAction,
    Comment,
    COMMENT_ERROR_NOT_FOUND,
    CommentError,
    CommentState
} from './types';
import {AxiosInstance, AxiosResponse} from 'axios';

const url = "http://comment.cynthia.team";

const fetchCommentStart = (): FetchCommentAction => {
    return {
        type: FETCH_COMMENT
    }
};

const fetchCommentSuccess = (payload: Comment[]): FetchCommentSuccessAction => {
    return {
        type: FETCH_COMMENT_SUCCESS,
        payload
    }
};

const fetchCommentError = (error: CommentError): FetchCommentErrorAction => {
    return {
        type: FETCH_COMMENT_ERROR,
        error
    }
};

export const fetchComment = (postId: string) => {
    return (dispatch: Dispatch, getState: CommentState, interaxios: AxiosInstance) => {
        dispatch(fetchCommentStart());
        interaxios.get(`${url}/posts/${postId}/comments`)
            .then((res: AxiosResponse) => {
                const posts: Comment[] = [].concat(res.data);
                dispatch(fetchCommentSuccess(posts))
            })
            .catch(() => {
                dispatch(fetchCommentError(COMMENT_ERROR_NOT_FOUND));
            });
    }
};
