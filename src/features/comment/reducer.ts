import {CommentAction, CommentState} from './types';

const initialState: CommentState = {
    comments: [],
    isLoading: false
};

export const reducer = (state = initialState, action: CommentAction) => {
    switch (action.type) {
        case 'FETCH_COMMENT':
            return {
                ...state,
                isLoading: true
            };
        case 'FETCH_COMMENT_SUCCESS':
            return {
                comments: action.payload,
                isLoading: false
            };
        case 'FETCH_COMMENT_ERROR':
            return {
                comments: [],
                isLoading: false,
                error: action.error
            };
        default:
            return state;
    }
};
