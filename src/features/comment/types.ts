// Comment Type
export type Comment = {
    id: string,
    message: string,
    postId: string,
    authorId: string,
    createdAt: Date,
    updatedAt: Date
}

export type CommentState = {
    comments: Comment[],
    isLoading: boolean,
    error?: CommentError
}

// Errors
export type CommentError =
    | typeof COMMENT_ERROR_NOT_FOUND
    | typeof COMMENT_ERROR_UNAUTHORIZED
    | typeof COMMENT_ERROR_SERVER_ERROR

export const COMMENT_ERROR_NOT_FOUND = "COMMENT_ERROR_NOT_FOUND";
export const COMMENT_ERROR_UNAUTHORIZED = "COMMENT_ERROR_UNAUTHORIZED";
export const COMMENT_ERROR_SERVER_ERROR = "COMMENT_ERROR_SERVER_ERROR";

// Comment Actions
export type CommentAction =
    | FetchCommentAction
    | FetchCommentSuccessAction
    | FetchCommentErrorAction
    /*| SaveCommentAction
    | SaveCommentSuccessAction
    | SaveCommentErrorAction
    | DeleteCommentAction
    | DeleteCommentSuccessAction
    | DeleteCommentErrorAction*/

export type FetchCommentAction = {
    type: typeof FETCH_COMMENT
}

export type FetchCommentSuccessAction = {
    type: typeof FETCH_COMMENT_SUCCESS,
    payload: Comment[]
}

export type FetchCommentErrorAction = {
    type: typeof FETCH_COMMENT_ERROR,
    error: CommentError
}

export const FETCH_COMMENT = "FETCH_COMMENT";
export const FETCH_COMMENT_SUCCESS = "FETCH_COMMENT_SUCCESS";
export const FETCH_COMMENT_ERROR = "FETCH_COMMENT_ERROR";

/*export const SAVE_COMMENT = "SAVE_COMMENT";
export const SAVE_COMMENT_SUCCESS = "SAVE_COMMENT_SUCCESS";
export const SAVE_COMMENT_ERROR = "SAVE_COMMENT_ERROR";

export const DELETE_COMMENT = "DELETE_COMMENT";
export const DELETE_COMMENT_SUCCESS = "DELETE_COMMENT_SUCCESS";
export const DELETE_COMMENT_ERROR = "DELETE_COMMENT_ERROR";*/
