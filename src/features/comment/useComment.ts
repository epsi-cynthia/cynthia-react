import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {State} from 'store';
import {fetchComment} from './actions';

export const useComment = () => {
    const dispatch = useDispatch();
    const comments = useSelector((state: State) => state.comment.comments);
    const isLoading = useSelector((state: State) => state.comment.isLoading);
    const error = useSelector((state: State) => state.comment.error);

    return {
        comments,
        commentIsLoading: isLoading,
        commentError: error,
        fetchComment: (postId: string) => dispatch(fetchComment(postId))
    };
};
