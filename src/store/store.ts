import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import {interaxios} from 'utils/axios';
import {reducer as commentReducer} from 'features/comment';
import {reducer as postReducer} from 'features/post';

const reducers = combineReducers({
    comment: commentReducer,
    post: postReducer
});

export const store = createStore(reducers, applyMiddleware(thunk.withExtraArgument(interaxios)));

export type State = ReturnType<typeof reducers>;
